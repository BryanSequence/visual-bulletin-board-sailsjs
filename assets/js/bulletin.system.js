
/**
 * 
 * @author Bryan Judelle Ramos
 * @description
 * 
 */
var uTable = $('#user-api-table');
var gToken = window.localStorage.getItem("token");
Bulletin.App.System = {

    fnLoader: function() {
        var self = this;
        uTable.DataTable();
        self.addUserModalHandler();
        self.loadUserTableData();
    },

     /**
     * 
     * Add new user button handler
     * 
     */
    addUserModalHandler: function() {
        var self = this;
        $('#btn-add-user').click(function() {
            $('#modal-form-user').modal({show: true});
        });

        /**
         * handle add event on modal
         */
        $('#btnAddUserSubmit').on('click', function(e) {
           
            var params = {
                urlMethod : 'POST',
                apiURL    : '/admin/save-user',
                data      : AdminUtils.constructPayloadData('#form-add-user')
            };

            AdminUtils.sendHTTPRequest(params, function(xhrResult) {
                console.log(JSON.stringify(xhrResult));
                if (xhrResult[0].status === 200) {
                    $('#modal-form-user').modal('hide');
                    $.notify("New User Added Sucessfully!", "success");
                    self.loadUserTableData();
                }
                else {
                    alert('Error! Please Contact Your System Administrator');
                    console.log(xhrResult[0].responseText);
                }
            });
            e.preventDefault();
        });
    },

    /**
     * 
     * Edit user button handler
     * 
     */
    editUserModalHandler: function(context, e) {
        var self   = this,
        modalEdit  = $('#modal-edit-user'),
        btnEdit    = $('#btnUserEventSubmit'),
        editRole   = $('#editUserRole'),
        editStatus = $('#editUserStatus'),
        editBStatus= $('#badgeStatus'),
        editBRole  = $('#badgeRole'),
        editURL    = '/admin/get-user/',
        paramId    = $(context).data('value');    

        modalEdit.modal({show: true});

        self.getJSONCallback(editURL + paramId, false, function(data) {
            $('#editUserFullname').val(data.fullname);
            $('#editUserName').val(data.username);

            // -- User Status -- //
            editBStatus.removeAttr('class').html('');
            editBRole.removeAttr('class').html('');

            if (data.status === 'ACTIVE') {
                editStatus.prop('checked', true).iCheck('update');
                editBStatus.addClass('label label-success').append('<i class="fa fa-check-circle"></i> &nbsp;&nbsp;&nbsp;ACTIVE');
            } else {
                editStatus.prop('checked', false).iCheck('update');
                editBStatus.addClass('label label-default').append('<i class="fa fa-ban"></i> &nbsp;&nbsp;&nbsp;DISABLED');
            }
            // -- User Role -- //
            if (data.role === 'ADMIN') {
                editRole.prop('checked', true).iCheck('update');
                editBRole.addClass('label label-primary').append('<i class="fa fa-user-o"></i> &nbsp;&nbsp;&nbsp;ADMIN');
            } else {
                editRole.prop('checked', false).iCheck('update');
                editBRole.addClass('label label-warning').append('<i class="fa fa-user"></i> &nbsp;&nbsp;&nbsp;USER');
            }
        });

        editStatus.on('ifChecked', function(e) {
            editBStatus.removeAttr('class')
                       .html('')
                       .addClass('label label-success')
                       .append('<i class="fa fa-check-circle"></i> &nbsp;&nbsp;&nbsp;ACTIVE');
        }).on('ifUnchecked', function(e) {
            editBStatus.removeAttr('class')
                       .html('')
                       .addClass('label label-default')
                       .append('<i class="fa fa-ban"></i> &nbsp;&nbsp;&nbsp;DISABLED');
        });

        editRole.on('ifChecked', function(e) {
            editBRole.removeAttr('class')
                     .html('')
                     .addClass('label label-primary')
                     .append('<i class="fa fa-user-o"></i> &nbsp;&nbsp;&nbsp;ADMIN');
        }).on('ifUnchecked', function(e) {
            editBRole.removeAttr('class')
                     .html('')
                     .addClass('label label-warning')
                     .append('<i class="fa fa-user"></i> &nbsp;&nbsp;&nbsp;USER');
        });

        btnEdit.off('click').on('click', function (e) {
            console.log(AdminUtils.constructPayloadData('#form-edit-user'));
            var params = {
                urlMethod : 'POST',
                apiURL    : '/admin/update-user/'+ paramId,
                data      : AdminUtils.constructPayloadData('#form-edit-user'),
            };

            AdminUtils.sendHTTPRequest(params, function(xhrResult) {
                console.log(JSON.stringify(xhrResult));
                if (xhrResult[0].status === 200) {
                    $('#modal-edit-user').modal('hide');
                    self.loadUserTableData();
                    $.notify("User Information Updated Sucessfully!", "success");
                }
                else
                    alert('Error! Please contact your system administrator');
            });

            e.preventDefault();
        });
    },

    /**
     * 
     * Delete user button handler
     * 
     */
    deleteUserModalHandler: function(context, ev) {
        var self        = this,
            modalDelete = $('#modal-delete-user'),
            deleteURL   = '/admin/delete-user/',
            getURL      = '/admin/get-user/',
            paramId     = $(context).data('value'),
            deleteFullName = $('#deleteFullName'),
            deleteUserName = $('#deleteUserName'),
            deleteRole     = $('#deleteRole'),
            deleteStatus    = $('#deleteStatus');

            modalDelete.modal({show: true});
            
            self.getJSONCallback(getURL + paramId, false, function(data) {
                

                    deleteFullName.text(data.fullname);
                    deleteUserName.text(data.username);

                    deleteRole.removeAttr('class').html('');
                    deleteStatus.removeAttr('class').html('');

                    if (data.status === 'ACTIVE') {
                        deleteStatus.addClass('label label-success').append('<i class="fa fa-check-circle"></i> &nbsp;&nbsp;&nbsp;ACTIVE');
                    } else {
                        deleteStatus.addClass('label label-default').append('<i class="fa fa-ban"></i> &nbsp;&nbsp;&nbsp;DISABLED');
                    }
                    // -- User Role -- //
                    if (data.role === 'ADMIN') {
                        deleteRole.addClass('label label-primary').append('<i class="fa fa-user-o"></i> &nbsp;&nbsp;&nbsp;ADMIN');
                    } else {
                        deleteRole.addClass('label label-warning').append('<i class="fa fa-user"></i> &nbsp;&nbsp;&nbsp;USER');
                    }
                   
            });

            $('#btnDeleteUser').off('click').on('click', function(e) {
                var params = {
                    urlMethod : 'POST',
                    apiURL    : deleteURL + paramId,
                    data      : {userStatus: 'REMOVED'},
                };
    
                AdminUtils.sendHTTPRequest(params, function(xhrResult) {
                    if (xhrResult[0].status === 200) {
                        $('#modal-delete-user').modal('hide');
                        $.notify("User Removed Successfully!", "info");
                        self.loadUserTableData();
                    }
                    else
                        alert('Error! Please contact your system administrator');
                });
                e.preventDefault();
            });
    },

    loadUserTableData: function() {
        var self       = this,
            statusFlag = null;

            self.getJSONCallback('/admin/get-all-users', true, function(data) {
                if ($.fn.dataTable.isDataTable(uTable)) uTable.DataTable().destroy();
                uTable.dataTable({
                    responsive: true,
                    data: data,
                    aaSorting: [[ 4, "desc" ]],
                    aoColumns: [
                        {sName: data[0], mRender: 
                            function(d) {
                                return d;  
                            },
                        },
                        {sName: data[1], mRender: 
                            function(d) {
                                return d;
                            }
                        },
                        {sName: data[2], mRender: 
                            function(d) {
                                return d;
                            }
                        },
                        {sName: data[3], mRender: 
                            function(d) {
                                var disabled  = '<span class="label label-default"><i class="fa fa-ban"></i> &nbsp;&nbsp;&nbsp;DISABLED</span>';
                                var active = '<span class="label label-success"><i class="fa fa-check-circle"></i> ACTIVE</span>';
                                
                                if (d === 'ACTIVE') {
                                    statusFlag = 'ACTIVE';
                                    return active;
                                } else {
                                    statusFlag = 'DISABLED';
                                    return disabled;
                                }
                            }
                        },                
                        {sName: data[4], mRender: 
                            function(d) {
                                var btnEditDOM   = '<button class="btn btn-primary btn-xs btnEdit" data-title="Edit" data-target="#edit" data-value="'+ d +'" onClick="Bulletin.App.System.editUserModalHandler(this, event);"><span class="glyphicon glyphicon-pencil"></span></button>&nbsp;',
                                    btnDeleteDOM = '<button class="btn btn-danger btn-xs btnDelete" data-title="Delete" data-target="#delete" data-value="'+ d +'" onClick="Bulletin.App.System.deleteUserModalHandler(this, event)"><span class="glyphicon glyphicon-trash"></span></button>';
                        
                                return btnEditDOM + btnDeleteDOM;
                            }
                        },
                    ]
                });
            });
    },

    getJSONCallback: function(apiURL, dataTableFlag,  callback) {
        var self = this,
            url  = apiURL + '?token=' + gToken;
        $.getJSON(url, function (list) {
            if (dataTableFlag === true) {
                var objData = self.constructUserObject(list);
                callback(objData);
            } else {
                callback(list);
            }
        });
    },
    
    /**
     * ConstructEventObject for Datatable
     *  arrange data from API into acceptable datatable format
     * 
     */
    constructUserObject: function(data) {
        var arrList = [];

        for (var i in data) {
            var curArr = [];
            if (data[i].status !== 'REMOVED') {
                curArr.push(data[i].fullname);
                curArr.push(data[i].username);
                curArr.push(data[i].role);
                curArr.push(data[i].status);
                curArr.push(data[i].userId);
                arrList.push(curArr);
            }
        }
        return arrList;
    },
};

(function() {
    Bulletin.App.System.fnLoader();
})();