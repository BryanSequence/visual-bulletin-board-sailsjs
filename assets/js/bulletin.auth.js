/**
 * 
 * @author Bryan Judelle Ramos
 * @description
 * 
 */

Bulletin.App.Auth = {
    login: function() {
        var btnLogin = $('#btnLogin'),
            userName = $('#username'),
            passWord = $('#password');

        btnLogin.on('click', function(e) {
            var obj = {
                username: userName.val(),
                password: passWord.val()
            },
            params = {
                urlMethod : 'POST',
                apiURL    : '/auth/',
                data      : obj,
            };

            AdminUtils.sendHTTPRequest(params, function(xhrResult) {
                console.log(JSON.stringify(xhrResult));
                
                if (xhrResult.auth_token) {
                    $('#modal-edit-user').modal('hide');
                    window.localStorage.setItem("token", xhrResult.auth_token);
                    window.localStorage.setItem("role", xhrResult.role);
                    window.localStorage.setItem("fullname", xhrResult.fullname);
                    var url = '/admin?token=' + xhrResult.auth_token;
                    window.location.replace(url);
                }
                else
                    alert('Invalid Username or Password');
            });
            e.preventDefault();
        });
    },
};
(function() {
    Bulletin.App.Auth.login();
})();