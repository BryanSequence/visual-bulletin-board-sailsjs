/**
 * 
 * @author Bryan Judelle Ramos
 * @description
 * 
 */
var oTable = $('#api-table');
var gToken = window.localStorage.getItem("token");
var gRole  = window.localStorage.getItem("role");

Bulletin.App.Admin = {
    
    fnDashboardLoader: function() {
        var self = this;

            self.addEventModalHandler();
            self.loadEventTableData();

            $('a.admin-nav').prop('href', '/admin?token=' + gToken);
            $('a.pending-nav').prop('href', '/admin/pending-events?token=' + gToken);
            $('a.preview-nav').prop('href', '/admin/event-preview?token=' + gToken);
            $('a.system-nav').prop('href', '/admin/system-management?token=' + gToken);

            // if (gRole !== undefined && gRole === 'ADMIN')
            //     $('.adminApproval').removeClass('hide');
            // else 
            //     $('.adminApproval').addClass('hide');

            $('#logout').on('click', function(e) {
                window.localStorage.clear();
                window.location.replace('/login');
            });
        },

    /**
     * 
     * 
     * 
     */
    editEventModalhandler: function (context, e) {
        var self       = this,
            modalEdit  = $('#modal-edit'),
            editStatus = $('#editEventStatus'),
            paramId    = $(context).data('value');

            modalEdit.modal({show: true});

            //call off event to prevent propagation of multiple notifyjs box
            $('#btnEditEvent').off('click').on('click', function(e) {
            
                var params = {
                    urlMethod : 'POST',
                    apiURL    : '/api/update-slide/'+ paramId,
                    data      : {status: 'APPROVED'},
                };
    
                AdminUtils.sendHTTPRequest(params, function(xhrResult) {
                    if (xhrResult[0].status === 200) {
                        $('#modal-edit').modal('hide');
                        self.loadEventTableData();
                        $.notify("Slide Updated Sucessfully!", "success");
                    }
                    else
                        alert('Error! Please contact your system administrator');
                });

                e.preventDefault();
            });
    },


    /**
     * 
     * Delete Event Modal
     * 
     */
    deleteEventModalHandler: function(context, event) {
        var self        = this,
            modalDelete = $('#modal-delete'),
            deleteURL   = '/api/delete-slide/',
            paramId     = $(context).data('value');

            modalDelete.modal({show: true});
            

            $('#btnDeleteEvent').off('click').on('click', function(e) {
                var params = {
                    urlMethod : 'POST',
                    apiURL    : deleteURL + paramId,
                    data      : {status: 'REMOVED'},
                };
    
                AdminUtils.sendHTTPRequest(params, function(xhrResult) {
                    if (xhrResult[0].status === 200) {
                        $('#modal-delete').modal('hide');
                        $.notify("Event Removed Sucessfully!", "info");
                        self.loadEventTableData();
                    }
                    else
                        alert('Error! Please contact your system administrator');
                });
                e.preventDefault();
            });
    },

    /**
     * 
     * Add new event button handler
     * 
     * 
     */
    addEventModalHandler: function() {
        var self = this;

        $('#btn-add-event').click(function() {
            $('#modal-form').modal({show: true});
        });

        $('#form-add-event').on('submit', function(e) {
            e.preventDefault();
          
            var formData = new FormData(this);
            var params = {
                urlMethod: 'POST',
                apiURL: 'api/save-slide',
                formData: formData
            };
        
            AdminUtils.submitFormDataRequest(params, function (xhrResult) {
                console.log(xhrResult);
                if (xhrResult.message === "success") {
                    $('#modal-form').modal('hide');
                    $.notify("New Event Added Sucessfully!", "success");
                    self.loadEventTableData();
                }
                else {
                    alert('Error! Please Contact Your System Administrator');
                    console.log(xhrResult);
                }
            });
        })
    },

    loadEventTableData: function() {
        var self      = this,
            statusFlag = null,
            currURL    = null;
            
        self.getJSONCallback('/api/get-all-slide', true, function(data) {
            if ($.fn.dataTable.isDataTable(oTable)) oTable.DataTable().destroy();
            oTable.dataTable({
                responsive: true,
                data: data,
                aoColumns: [
                    {mData: "title", mRender: 
                        function(d) {
                            return '<b>' + d + '</b>';  
                        },
                    },
                    {mData: "status" , mRender: 
                        function(d) {
                            var pending  = '<span class="label label-warning"><i class="fa fa-clock-o"></i> &nbsp;&nbsp;&nbsp;PENDING</span>';
                            var approved = '<span class="label label-success"><i class="fa fa-check"></i> APPROVED</span>';
                            var removed  = '<span class="label label-danger"><i class="fa fa-times"></i> &nbsp;&nbsp;&nbsp;REMOVED</span>';
                            
                            if (d === 'APPROVED') {
                                statusFlag = 'APPROVED';
                                return approved;
                            } else if (d === 'PENDING') {
                                statusFlag = 'PENDING';
                                return pending;
                            } else {
                                statusFlag = 'REMOVED';
                                return removed;
                            }
                        }
                    },
                    {mData: "createdBy", mRender: 
                        function(d) {
                            return d;
                        } 
                    },                 
                    {mData: "id", mRender: 
                        function(d) {
                            // var btnEditDOM = '<button class="btn btn-primary btn-xs btnEdit" data-title="Edit" data-target="#edit" data-value="' + d +'" onClick="Bulletin.App.Admin.editEventModalHandler(this, event);"><span class="glyphicon glyphicon-eye-open"></span></button>&nbsp;',
                            var btnEditDOM = '<button class="btn btn-primary btn-xs btnEdit" data-title="Edit" data-target="#edit" data-value="' + d + '" onClick="Bulletin.App.Admin.editEventModalhandler(this, event);"><span class="glyphicon glyphicon-pencil"></span></button>&nbsp;',
    
                            btnDeleteDOM = '<button class="btn btn-danger btn-xs btnDelete" data-title="Delete" data-target="#delete" data-value="'+ d +'" onClick="Bulletin.App.Admin.deleteEventModalHandler(this, event);"><span class="glyphicon glyphicon-trash"></span></button>';
                    
                            if (statusFlag === 'REMOVED') 
                                return btnEditDOM;
                            else if (statusFlag === 'APPROVED')
                                return btnDeleteDOM;
                            else
                                return btnEditDOM + btnDeleteDOM;
                        }
                    },
                ]
            });
        });
    },

    getJSONCallback: function(apiURL, dataTableFlag,  callback) {
        var self = this,
            url  = apiURL;
        $.getJSON(url, function (list) {
                callback(list);
        });
    },
};


(function() {
    Bulletin.App.Admin.fnDashboardLoader();
})();