/**
 * AdminUtils Object, Return global scope function
 * 
 * @author Bryan Judelle Ramos
 * 
 * 
 */

var AdminUtils = {
    hello: function() {
        return 'hello worldxxx';
    },

    /**
     * 
     * @description dynamically construct data object array for datatable
     * 
     */
    constructDataObject: function(data) {
        var arrList = [];
        for (var i in data) {
            var key     = Object.keys(data[i]),
                val     = data[i],
                currArr = [];

            for (var j in key) currArr.push(val[key[j]]);
           
            arrList.push(currArr);
        } 
        return arrList;
    },

    /**
     * 
     * @description construct payload data for AJAX HTTP Request
     * @param formDOM    - REST Services URL
     * 
     */
    constructPayloadData: function(formDOM) {
        var frmData     = JSON.stringify($(formDOM).serializeArray()),
            objArr      = {},
            parseObjArr = JSON.parse(frmData);
        
        for (var i in parseObjArr) {
            objArr[parseObjArr[i].name] = parseObjArr[i].value;
        }
        objArr.user = window.localStorage.getItem("fullname") ? window.localStorage.getItem("fullname") : "N/A";
        return objArr;
    },

     /**
     * 
     * @description Re-usable HTTP request method
     * 
     * @argument apiURL    - REST Services URL
     * @argument data      - serialized data (json)
     * @argument urlMethod - Request Method Type [POST, PUT, GET, DELETE]
     * @argument callback  - callback function, return data from server
     * 
     */
    sendHTTPRequest: function(opts, callback) {
        $.ajax({
            url: opts.apiURL,
           type: opts.urlMethod,
           data: JSON.stringify(opts.data),
           headers: {"Authorization": "Bearer " + localStorage.getItem('token')},
           contentType: 'application/json',
              dataType: 'json',
                 cache: false, 
           success: function(data){
              return callback(data);
           },
           error: function(jqXHR, textStatus, err){
               return callback([jqXHR, textStatus, err]);
           } 
       })
    },

    submitFormDataRequest: function(opts,callback) {
        
        $.ajax({
            url: opts.apiURL,
            type: opts.urlMethod,
            data: opts.formData,
            headers: { "Authorization": "Bearer " + localStorage.getItem('token') },
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {
                return callback(data);
            },
            error: function (jqXHR, textStatus, err) {
                return callback([jqXHR, textStatus, err]);
            }
        })
    }

};