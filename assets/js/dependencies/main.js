var Bulletin = {
    App : {},
};


$('#calendar').fullCalendar({
    header: {
        left: '',
        center: 'title',
        right: ''
    },
    contentHeight: 220

    //height: $(window).height() * 0.40,
});


$(document).ready(function() {
    
    $('#homeCarousel').carousel({
        interval: 5000,
    });

    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass   : 'iradio_flat-green'
    });
    var token = localStorage.getItem('token');
    $.getJSON('api/get-all-slide?token=' + token, function (list) {
   
        for (var i in list) {
            
             if (list[i].status === 'APPROVED') {
                 if (list[i].file_type === 'image/png' || list[i].file_type === 'image/jpeg' || list[i].file_type === 'image/jpg') {
                    var content = '<div class="item" data-type="' + list[i].file_type +'"><img src="' + list[i].file_url + '" /></div>';
                    $('#carouselBody').append(content);
                }

                if (list[i].file_type === 'video/mp4') {
                    var contentVid = '<div class="item" data-type="' + list[i].file_type + '" data-id="' + list[i].id + '"><div align="center" class="embed-responsive embed-responsive-16by9"><video id="myVideo_' + list[i].id +'" class="embed-responsive-item"><source src="' + list[i].file_url +'" type="video/mp4"></video></div></div>';
                    $('#carouselBody').append(contentVid);
                }
            }
        }
    });

    $('#homeCarousel').on('slid.bs.carousel', function (e) {
        var elem = $(this).find('.item.active').data('type');
        
        if (elem === 'video/mp4') { 
            var id = $(this).find('.item.active').data('id');
            var vidName = '#myVideo_' + id;
            $(vidName)[0].play();
        }
    }); 

    $('video').on('play start', function (e) {
        $('#homeCarousel').carousel('pause');
    });
    $('video').on('stop pause ended', function (e) {
        $("#homeCarousel").carousel({ interval: 5000 });
    });

    $('#btnSaveFlashReport').on('click', function(e) {
        
        window.localStorage.setItem("flashreport", $('#flashReport').val());
        alert('flash report saved!');

        var flash = localStorage.getItem('flashreport'); //? localStorage.getItem('flashreport') : 'Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor!';
        $('#flashreport-banner').html(flash);
    });
    
    var flash = localStorage.getItem('flashreport'); //? localStorage.getItem('flashreport') : 'Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor!';
    $('#flashreport-banner').html(flash);
    
    /**
     * 
     * @author Bryan Judelle Ramos
     * 
     */
    function clock() {
        $('.clock').html(new Date().toLocaleTimeString());        
    }
    setInterval(clock, 1000);    
});