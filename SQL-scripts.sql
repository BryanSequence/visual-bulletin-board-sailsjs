
CREATE SCHEMA IF NOT EXISTS `visual_bulletin_db` ;


CREATE TABLE IF NOT EXISTS `visual_bulletin_db`.`tbl_vision_mission` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `mission` TEXT NOT NULL,
  `vision` TEXT NOT NULL,
  `dateCreated` DATETIME NOT NULL DEFAULT NOW(),
  `dateUpdated` DATETIME NOT NULL DEFAULT NOW(),
  `createdBy` VARCHAR(45) NOT NULL DEFAULT 'N/A',
  PRIMARY KEY (`id`));


CREATE TABLE IF NOT EXISTS `visual_bulletin_db`.`tbl_flash_report` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `flash_report_title` VARCHAR(255) NOT NULL,
  `flash_report_content` TEXT NOT NULL,
  `status` ENUM('PENDING', 'APPROVED', 'REMOVED') NOT NULL DEFAULT 'PENDING',
  `dateCreated` DATETIME NOT NULL DEFAULT NOW(),
  `dateUpdated` DATETIME NOT NULL DEFAULT NOW(),
  `createdBy` VARCHAR(45) NOT NULL DEFAULT 'N/A',
  PRIMARY KEY (`id`));


CREATE TABLE IF NOT EXISTS `visual_bulletin_db`.`tbl_bulletin_board` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `file_url` VARCHAR(355) NOT NULL,
  `file_type` VARCHAR(45) NULL,
  `status` ENUM('PENDING', 'APPROVED', 'REMOVED') NOT NULL DEFAULT 'PENDING',
  `dateCreated` DATETIME NOT NULL DEFAULT NOW(),
  `dateUpdated` DATETIME NOT NULL DEFAULT NOW(),
  `createdBy` VARCHAR(45) NOT NULL DEFAULT 'N/A',
  PRIMARY KEY (`id`));


CREATE TABLE IF NOT EXISTS `tbl_users` (
  `userId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` enum('ADMIN','USER','REMOVED') NOT NULL DEFAULT 'USER',
  `status` enum('ACTIVE','DISABLED','REMOVED') NOT NULL DEFAULT 'ACTIVE',
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_users` WRITE;

INSERT INTO `tbl_users` VALUES (1,'ADMIN2','ADMINISTRATOR','$2a$10$SggfzCNeY9ZUvi6hg7.Zf.a6/JFZo3s98L54IneD.DHNoOlJC/akC','ADMIN','DISABLED'),(2,'user1','user1','$2a$10$aKcGIRe/RDZJYuidQru3nO8SWKA0ykyxZkZC2zwSxFvu7OJoKGT2e','USER','ACTIVE'),(3,'test','test','$2a$10$XSfK/bDS/hDk28FQvCcTy.TLMBASD7YQNUPTvZkynbbGs/rPMxnA.','ADMIN','ACTIVE');

UNLOCK TABLES;