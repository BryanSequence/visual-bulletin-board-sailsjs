/**
 * Bulletin.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  autoCreatedAt: false,
  autoUpdatedAt: false,
  connection: 'mysql',
  tableName: 'tbl_bulletin_board',
  autoPK: false,
  attributes: {
    id: {
      type: 'integer',
      unique: true,
      primaryKey: true,
      autoIncrement: true,
    },
         title: { type: 'string' },
     file_url: { type: 'string' },
    file_type: { type: 'string' },
       status: {
         type: 'string',
         enum: ['PENDING', 'APPROVED', 'REMOVED']
    },
    createdBy: {  type: 'string'}
  }
};

