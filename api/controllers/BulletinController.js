/**
 * BulletinController
 *
 * @description :: Server-side logic for managing Bulletins
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var _ = require('underscore'),
 path = require('path');

module.exports = {

    saveBulletinInfo: function (req, res) {
        var params = _.extend(req.query || {}, req.params || {}, req.body || {});
        var allowedFile = ['video/mp4', 'video/avi', 'image/jpeg', 'image/png'];
        var fileType    = '';

        if (req.method === 'GET') return res.json({ 'status': 'GET not allowed' });

        var protocol = req.connection.encrypted ? 'https' : 'http',
            base_url = protocol + '://' + req.headers.host + '/',
            uploadFile = req.file('upload_file');

        uploadFile.upload({
            dirname: process.cwd() + sails.config.globals.FILE_UPLOAD_PATH,
            saveAs: function (__newFileStream, cb) {
                if (allowedFile.indexOf(__newFileStream.headers['content-type']) === -1) 
                    return res.serverError({ error_message: 'Invalid File' });
                else {
                    cb(null, new Date().getTime() + '_' + 'file' + path.extname(__newFileStream.filename));
                    fileType = allowedFile[allowedFile.indexOf(__newFileStream.headers['content-type'])];
                }
                   
            },

        }, function onUploadComplete(err, files) {

            if (err) return res.serverError(err);

            var filename = files[0].fd.substring(files[0].fd.lastIndexOf('/') + 1),
                uploadLocation = process.cwd() + sails.config.globals.FILE_UPLOAD_PATH + filename,
                tempLocation   = process.cwd() + sails.config.globals.TMP_FILE_UPLOAD_PATH + filename,
                file_url       = base_url + ((filename.indexOf(sails.config.globals.PROJ_ASSET_PATH) >= 0) ?
                                 filename.split(sails.config.globals.PROJ_ASSET_PATH)[1].split("\\").join("/")
                                : "files/uploads/" + filename);

            if (!file_url) return res.serverError('file url is missing');

            var fileObj = {
                   title: (params.title) ?  params.title : 'No title',
                file_url: file_url,
               file_type: fileType,
                  status: (params.bulletinStatus === 'on') ? 'APPROVED' : 'PENDING'
            };

            BulletinServices.saveBulletinInfo(fileObj, function(bulletin) {
                bulletin.message = "success";
                return res.json(bulletin);
            });
        });
    },

    updateSlide: function (req, res) {
        var params = _.extend(req.query || {}, req.params || {}, req.body || {});

        var paramId = { id: params.id },
            paramObj = { status: params.status };

        var response = BulletinServices.updateBulletinInfo(paramId, paramObj, function (err, slide) {
            if (err) return err;
            return slide;
        });

        return res.json(response);
    },

    deleteSlide: function (req, res) {
        var params = _.extend(req.query || {}, req.params || {}, req.body || {});

        var paramId = { id: params.id },
            paramObj = { status: params.status };

        var response = BulletinServices.updateBulletinInfo(paramId, paramObj, function (err, slide) {
            if (err) return err;
            return slide;
        });

        return res.json(response);
    },

    
};

