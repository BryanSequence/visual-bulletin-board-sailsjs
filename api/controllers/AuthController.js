/**
 * AuthController
 *
 * @description :: Server-side logic for managing Auths
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var _ = require('underscore');

module.exports = {
    login: function (req, res) {
        var params = _.extend(req.query || {}, req.params || {}, req.body || {});

        var user = params.username,
            pass = params.password,
            paramsObj = {
                username: user
            };

        var result = UserServices.findUserByQuery(paramsObj, function (results) {

            if (results !== undefined) {

                if (results.error !== undefined && results.error) return res.json(results);

                var validateLogin = Utils.decryptPassword(pass, results.password);

                if (validateLogin) {

                    var token = Utils.issue({ id: results.userId }),
                        userId = results.userId,
                        userRole = results.role;
                    userFullname = results.fullname;
                   
                    return res.json({ userId: userId, fullname: userFullname, role: userRole, auth_token: token });
                } else {
                    return res.json(401, {
                        err: 'Invalid username or password'
                    });
                }
            } else {
                return res.json(401, { err: 'Invalid username or password' });
            }

        });
    },
};

