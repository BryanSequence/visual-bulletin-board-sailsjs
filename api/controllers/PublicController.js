/**
 * PublicController
 *
 * @description :: Server-side logic for managing Publics
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    fetchAllBulletinData: function (req, res) {
        var response = BulletinServices.getAllBulletin(function (bulletin) {
            return res.json(bulletin);
        });
    },

};

