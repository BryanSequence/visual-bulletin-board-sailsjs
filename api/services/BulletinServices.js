/**
 * 
 * BulletinServices.js
 * 
 * @author Bryan Judelle Ramos
 * @description Bulletin Services, do basic DB related query and passed to controller
 *              this contains all the DATA MODEL ABSTRACTION LAYER ONLY.
 * 
 * 
 */

module.exports = {

    findBulletingById: function (id, callback) {
        var params = { userId: id };
        Bulletin.findOne(params).exec(function (err, bulletinInfo) {
            if (err) {
                var opts = {
                    error: true,
                    origin: 'BulletinServices.findBulletingById {ERROR on FINDING BULLETIN INFO using Event Id}',
                    message: Utils.parseError(err)
                };
                return callback(opts);
            }
            return callback(bulletinInfo);
        });
    },

    findBulletinByQuery: function (query, callback) {
        Bulletin.findOne(query).exec(function (err, bulletinInfo) {
            if (err) {
                var opts = {
                    error: true,
                    origin: 'BulletinServices.findBulletinByQuery {ERROR on FINDING BULLETIN INFO using QUERY}',
                    message: Utils.parseError(err)
                };
                return callback(opts);
            }
            return callback(bulletinInfo);
        });
    },

    getAllBulletin: function (callback) {
        Bulletin.find().exec(function (err, listBulletin) {
            if (err) {
                var opts = {
                    error: true,
                    origin: 'BulletinServices.getAllBulletin {ERROR on RETRIEVING BULLETIN LIST}',
                    message: Utils.parseError(err)
                };
                return callback(opts);
            }
            return callback(listBulletin);
        });
    },

    saveBulletinInfo: function (params, callback) {
        Bulletin.create(params).exec(function (err, bulletinInfo) {
            if (err) {
                var opts = {
                    error: true,
                    origin: 'UserServices.saveBulletinInfo {ERROR on SAVING BULLETIN INFO}',
                    message: Utils.parseError(err)
                };
                return callback(opts);
            }
            else
                return callback(bulletinInfo);
        });
    },

    updateBulletinInfo: function (paramsId, paramsValue, callback) {
        Bulletin.update(paramsId, paramsValue).exec(function (err, bulletinInfo) {
            if (err) {
                var opts = {
                    error: true,
                    origin: 'BulletinServices.updateBulletinInfo {ERROR on UPDATING BULLETIN INFO}',
                    message: Utils.parseError(err)
                };
                return callback(opts);
            }
            return callback(bulletinInfo);
        });
    },
};
