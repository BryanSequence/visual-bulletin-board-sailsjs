/**
 * 
 * @author Bryan Judelle Ramos
 * @description A Stateless Utility/Helper for API's controller(s)
 * 
 */

var bcrypt = require('bcrypt'),
       jwt = require('jsonwebtoken'),
  tokenSecret = "secretissecet";

module.exports.encryptPassword = function(password) {
    return bcrypt.hashSync(password, 10);
},

module.exports.decryptPassword = function(password, hashedVal) {
    return bcrypt.compareSync(password, hashedVal);
},

module.exports.userNameCheck = function (un, callback) {
    UserServices.isUserNameExist(un, function(result) {
       return callback(result);
    });
},

module.exports.parseError = function(e) {
    return e.toString().substring(e.toString().indexOf('Details:  Error:'));
},

// Generates a token from supplied payload
module.exports.issue = function(payload) {
    return jwt.sign(
      payload,
      tokenSecret//, // Token Secret that we sign it with
    //   {
    //     expiresIn: 180 // Token Expire time
    //   }
    );
  },
  
  // Verifies token on a request
  module.exports.verify = function(token, callback) {
    return jwt.verify(
      token,       // The token to be verified
      tokenSecret, // Same token we used to sign
      {},          // No Option, for more see https://github.com/auth0/node-jsonwebtoken#jwtverifytoken-secretorpublickey-options-callback
      callback     //Pass errors or decoded token to callback
    );
  };